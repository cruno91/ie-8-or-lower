<?php
/**
 * @file ie8orlower.tpl.php
 *
 * This is a stub tpl file for the ie8orlower.
 *
 */
?>
<section id="ie-modal-wrapper">
  <section id="ie8-modal">
    <div style='clear: both; height: 112px; padding:0; position: relative;'>
      <a href="http://www.theie8countdown.com/ie-users-info">
        <img src="http://www.theie8countdown.com/assets/badge_iecountdown.png" border="0" height="112" width="348" alt="" />
      </a>
    </div>
  </section>
  <section id="ie7-modal">
    <div style='clear: both; height: 112px; padding:0; position: relative;'>
      <a href="http://www.theie7countdown.com/ie-users-info">
        <img src="http://www.theie7countdown.com/assets/badge_iecountdown.png" border="0" height="112" width="348" alt="" />
      </a>
    </div>
  </section>
  <section id="ie6-modal">
    <div style=' clear: both; height: 59px; padding:0 0 0 15px; position: relative;'>
      <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
        <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
      </a>
    </div>
  </section>
</section>
